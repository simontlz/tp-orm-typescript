/* eslint-disable import/extensions */
import Model, { ModelConfig } from './model';
import { Album } from './album';

export interface UserSchema {
  id: string|number;
  name: string;
  username: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: { lat: number; lng: number };
  };
  phone: string;
  website: string;
  company: { name: string; catchPhrase: string; bs: string };
}

export class User extends Model implements UserSchema {
  name!: string;

  username!: string;

  email!: string;

  address!: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: number;
      lng: number;
    };
  };

  phone!: string;

  website!: string;

  company!: {
    name: string;
    catchPhrase: string;
    bs: string;
  };

  albums?: Album[];

  static config: ModelConfig = {
    endpoint: 'users',
  };

  constructor(data: UserSchema) {
    super(data.id);
    Object.assign(this, data);
  }
}
