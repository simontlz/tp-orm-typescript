/* eslint-disable new-cap */
/* eslint-disable no-dupe-class-members */
import { NonFunctionKeys } from 'utility-types';
import { isNull } from 'util';

import api = require('../../api');

type SchemaOf<T extends object> = Pick<T, NonFunctionKeys<T>>;

type GenericType<T extends Model> = { new(data: any): T; config: ModelConfig};

enum QueryFilterOrder {
  Asc = 'asc',
  Desc = 'desc',
}

interface QueryFilter {
  where?: Record<string, any>;
  limit?: number;
  page?: number;
  sort?: string;
  order?: QueryFilterOrder;
}

export interface FindByIdOptions {
  includes: string[];
}

export type ModelIdType = number | string;

export enum RelationType {
  BelongsTo = 'belongsTo',
  HasMany = 'hasMany',
}

/**
 * Define the configuration of a relation
 */
export interface Relation {
  /** Type of the relation: hasMany, belongsTo, ... */
  type: RelationType;

  /** The target Model */
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  model: any;

  /**
   * The key containing the relation link
   * - on the target model if hasMany
   * - on the current model if belongsTo
   */
  foreignKey: string;
}

export interface ModelConfig {
  /**
   * The endpoint on the remote API, example 'users'
   */
  endpoint: string;

  /**
   * The definition of the relations
   */
  relations?: Record<string, Relation>;
}

export default abstract class Model {
  protected static config: ModelConfig;

  id: string | number;

  constructor(id: string | number) {
    this.id = id;
  }

  static async create<T extends Model>(dataOrModel: SchemaOf<T> | T): Promise<T[]> {
    const { data } = await api.default.post<T[]>(`${this.config.endpoint}`, dataOrModel);
    return data;
  }

  static async find<T extends Model>(filter?: QueryFilter): Promise<T[]> {
    const params: api.ApiParams = Model.handleFilters(filter);
    const { data } = await api.default.get<T[]>(`${this.config.endpoint}`, { params });
    return data;
  }

  static async findById<T extends Model>(this: GenericType<T>, id: ModelIdType, options?: FindByIdOptions): Promise<T> {
    const { data } = await api.default.get<SchemaOf<T>>(`${this.config.endpoint}/${id}`);
    const model = new this(data);
    if (options) {
      let relationsKeys: string[];
      const { relations } = this.config;
      if (relations) {
        relationsKeys = Object.keys(relations);
      }
      await Promise.all(options.includes.map(async (include) => {
        const relationFoundKey = relationsKeys.find((relationKey) => relationKey === include);
        if (relationFoundKey) {
          if (relations) {
            const relationUrl = Model.getUrl(model, relations[relationFoundKey], model.id);
            const relationResponse = await api.default.get(relationUrl);
            const typedRelationKey = relationFoundKey as keyof T;

            if (relationResponse.data.length) {
              const relationModels = relationResponse.data.map((relationData: any) => new relations[relationFoundKey].model(relationData));
              model[typedRelationKey] = relationModels;
            } else {
              const relationModel = new relations[relationFoundKey].model(relationResponse.data);
              model[typedRelationKey] = relationModel;
            }
          }
        }
      }));
    }
    return model;
  }

  static async updateById<T extends Model>(
    id: ModelIdType,
    data: Partial<SchemaOf<T>>
  ): Promise<T[]>;

  static async updateById<T extends Model>(model: T): Promise<T[]>;

  static async updateById<T extends Model>(
    idOrData?: ModelIdType | T,
    data?: Partial<SchemaOf<T>>,
  ): Promise<T[]> {
    if (typeof idOrData === 'object') {
      const response = await api.default.put<T[]>(`${this.config.endpoint}/${idOrData.id}`, idOrData);
      return response.data;
    }
    const response = await api.default.patch<T[]>(`${this.config.endpoint}/${idOrData}`, data);
    return response.data;
  }

  static async deleteById(id: ModelIdType): Promise<boolean> {
    const { data } = await api.default.delete(`${this.config.endpoint}/${id}`);
    if (Object.keys(data).length) {
      return false;
    }
    return true;
  }

  /**
   * Push changes that has occured on the instance
   */
  // save<T extends Model>(): Promise<T>;

  /**
   * Push given changes, and update the instance
   */
  // update<T extends Model>(data: Partial<SchemaOf<T>>): Promise<T>;

  /**
   * Remove the remote data
   */
  // remove(): Promise<void>;

  private static handleFilters(filters?: QueryFilter): api.ApiParams {
    const params: api.ApiParams = {};
    if (filters) {
      if (filters.where) {
        for (const whereFilter of Object.keys(filters.where)) {
          params[whereFilter] = filters.where[whereFilter];
        }
      }
      if (filters.limit) {
        params._limit = filters.limit;
      }
      if (filters.page) {
        params._page = filters.page;
      }
      if (filters.sort) {
        params._sort = filters.sort;
      }
      if (filters.order) {
        params._order = filters.order;
      }
    }

    return params;
  }

  private static getUrl<T extends Model>(baseModel: T, relation: Relation, id: number|string): string {
    let url = '';
    const foreignKey = relation.foreignKey as keyof T;
    switch (relation.type) {
      case RelationType.BelongsTo:
        url = `${relation.model.config.endpoint}/${baseModel[foreignKey]}`;
        break;
      case RelationType.HasMany:
        url = `${relation.model.config.endpoint}?${foreignKey}=${id}`;
        break;
      default:
        break;
    }

    return url;
  }
}
