/* eslint-disable import/extensions */
import { User } from './user';
import { Photo } from './photo';
import Model, {
  ModelConfig,
  RelationType,
} from './model';

export interface AlbumSchema {
  userId: number;
  id: string|number;
  title: string;
}

export class Album extends Model implements AlbumSchema {
  userId!: number;

  title!: string;

  photos: Photo[] = [];

  user?: User;

  static config: ModelConfig = {
    endpoint: 'albums',
    relations: {
      photos: {
        type: RelationType.HasMany,
        model: Photo,
        foreignKey: 'albumId',
      },
      user: {
        type: RelationType.BelongsTo,
        model: User,
        foreignKey: 'userId',
      },
    },
  };

  constructor(albumData: AlbumSchema) {
    super(albumData.id);
    Object.assign(this, albumData);
  }
}
