/* eslint-disable import/extensions */
import Model, { ModelConfig } from './model';

export interface PhotoSchema {
  albumId: number;
  id: string|number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

export class Photo extends Model implements PhotoSchema {
  albumId!: number;

  title!: string;

  url!: string;

  thumbnailUrl!: string;

  static config: ModelConfig = {
    endpoint: 'photos',
  };

  constructor(data: PhotoSchema) {
    super(data.id);
    Object.assign(this, data);
  }
}
