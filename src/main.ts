/* eslint-disable import/extensions */
import { Album } from './models/album';

async function run(): Promise<void> {
  try {
    // const album: Album = await Album.findById(8);
    // album.title = 'Album title changed with just the modellllll';
    // album.userId = 1;
    // Album.updateById(album);

    // Album.updateById(8, { title: 'Album title changed with just the modelou' } as Album);

    const albums: Album = await Album.findById<Album>(1, { includes: ['photos', 'user'] });
    console.log(albums);
  } catch (e) {
    console.log(e);
  }
}

run().catch((err) => {
  console.error(err);
});
